const state = {
    userInfo:{},
    companyInfo:{
        "product_id": '',
        "company_name": "",
        "contact": "",
        "tel": "",
        "address": "",
        "num": "",
        "company_id":""
    },
    issue_id:'',
    desc_id:0,
    proplem_steps:[],
    LingpeijianList:[],//零配件信息
    LingpeijianOrder:{},//零配件订单信息
    WhichLingpeijian:'',//零配件MAC
    DingdanType:'',//订单状态
    ProplemComponentsList:[],//确认问题零配件
    CeramPhoto:'',//拍照路径
    LingpeijianDingdanPhotoKeys:[],//订单照片
    LingpeijianDingdanVideoKeys:[],//订单视频
    NewsMsgs:true,//新消息
    LingpeijianOrderInfo:{}//零配件订单图片备注
}
export default state