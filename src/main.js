// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import Mint from 'mint-ui'
import axios from 'axios'
import Vuex from 'vuex'
import store from'./store'
import 'mint-ui/lib/style.css'
import $ from 'jquery'
import 'swiper/dist/css/swiper.css';

// import 'ml-ui/styles/index.css'
// import mlUI from 'ml-ui'
// Vue.use(mlUI)

//300ms延迟
import fastclick from 'fastclick'
import VueCordova from 'vue-cordova'
import MuseUI from 'muse-ui'
import 'mint-ui/lib/style.css'
import * as qiniu from 'qiniu-js'
//打开vConsole
// import VConsole from 'vconsole'
// let vConsole = new VConsole()
// Vue.use(vConsole)

import {post,fetch} from './apiconfig/index'
Vue.prototype.$post=post;
Vue.prototype.$fetch=fetch

import less from 'less'
Vue.use(less)

const load = require('load-script')
load('../src/sdk/NIM_Web_SDK_v5.8.0.js',(err, script) => {
    if (err) {
        console.log('LOAD NIM ERR:', err)
    } else {
        console.log('LOAD NIM SUCCESS', script.src)
    }
})

Vue.config.productionTip = false
Vue.use(VueCordova)
Vue.use(Mint)
Vue.use(Vuex)
Vue.use(MuseUI)
Vue.use(qiniu)
Vue.prototype.$axios = axios
//300ms延迟
fastclick.attach(document.body)

/* eslint-disable no-new */
// document.addEventListener("deviceready",function(){
    new Vue({
        el: '#app',
        router,
        store,
        components: { App },
        template: '<App/>',
        data: {
            eventHub: new Vue()
          }
      })
// },false)
//适配
var html = document.getElementsByTagName('html')[0];
function fontSize() {
    var w = document.documentElement.clientWidth
    w > 750 ? w = 750 : w = w;
    html.style.fontSize = w / 7.50 + 'px';
    // html.style.fontSize = w / 75 + 'px';
    console.log(html.style.fontSize)
}
fontSize()
window.onresize = function() {
    fontSize()
}
// 强制设置字体不受系统字体大小影响
document.addEventListener("deviceready", onDeviceReady, false);
function onDeviceReady() {
    if (window.MobileAccessibility) {
        window.MobileAccessibility.usePreferredTextZoom(false);
    }
    // console.log(1)
}
