import axios from 'axios'

axios.defaults.baseURL = ''
axios.defaults.timeout = 500000

//封装请求拦截
axios.interceptors.request.use(
    config => {
        // let token = localStorage.getItem('token')
        if(JSON.parse(localStorage.getItem('userInfo'))){
            let token = JSON.parse(localStorage.getItem('userInfo')).token
            config.headers['Content-Type'] = 'application/json;charset=UTF-8'
            config.headers['Authorization'] = ''
            if(token != null){
                config.headers['Authorization'] = token
            }
            return config
        }
        return config
    },
    error => {
        return Promise.reject(error)
    }
)

//封装响应拦截，判断token是否过期
axios.interceptors.response.use(
    response => {
        let {data} = response
        if (data.message === 'token failure!') {
            localStorage.removeItem('token')
        } else {
            return Promise.resolve(response)
        }
    },
    error => {
        return Promise.reject(error)
    }
)

//封装post请求
export function post(url,data = {}){
    return new Promise((resolve,reject) => {
      axios.post(url,data)
           .then(response => {
             resolve(response.data);
           },err => {
             reject(err)
           })
    })
  }

//封装get
export function fetch(url,params={}){
    return new Promise((resolve,reject) => {
        axios.get(url,{
            params:params
        }).then(response => {
            resolve(response.data);
        }).catch(err => {
            reject(err)
        })
    })
}